package com.alfabattle.task2.exception;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BaseRestException extends RuntimeException {
    private String status;

    public String getStatus() {
        return status;
    }
}