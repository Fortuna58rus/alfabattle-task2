package com.alfabattle.task2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class PaymentCategoryInfo {

    private BigDecimal min;
    private BigDecimal max;
    private BigDecimal sum;

    @JsonIgnore
    private int size;

}
