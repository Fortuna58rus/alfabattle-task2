package com.alfabattle.task2.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPaymentTemplates {

    private String recipientId;
    private Integer categoryId;
    private BigDecimal amount;
}
