package com.alfabattle.task2.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

@Data
public class UserPaymentAnalytic {

    private String userId;
    private BigDecimal totalSum;
    private Map<String, PaymentCategoryInfo> analyticInfo;
}
