package com.alfabattle.task2.dto;

import lombok.Data;

@Data
public class HealthDTO {

    private String status;
}
