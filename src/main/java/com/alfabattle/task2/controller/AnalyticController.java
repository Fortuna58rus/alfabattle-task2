package com.alfabattle.task2.controller;

import com.alfabattle.task2.model.UserPaymentAnalytic;
import com.alfabattle.task2.exception.NotFoundException;
import com.alfabattle.task2.model.KafkaPaymentInfo;
import com.alfabattle.task2.model.PaymentCategoryInfo;
import com.alfabattle.task2.model.UserPaymentStats;
import com.alfabattle.task2.model.UserPaymentTemplates;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/analytic")
@Slf4j
public class AnalyticController {

    private final ObjectMapper objectMapper;
    private final Multimap<String, KafkaPaymentInfo> usersPayments = HashMultimap.create();

    public AnalyticController(ObjectMapper mapper) {
        this.objectMapper = mapper;
    }

    @GetMapping("all")
    public Map<String, Collection<KafkaPaymentInfo>> getAllPayments() {
        return usersPayments.asMap();
    }

    @GetMapping
    public List<UserPaymentAnalytic> getAllUsers() {
        List<UserPaymentAnalytic> infos = new ArrayList<>();
        for (String userId : usersPayments.keySet()) {
            infos.add(getUserPaymentAnalytic(userId));
        }

        return infos;
    }

    @GetMapping("{userId}")
    public UserPaymentAnalytic getUserInfo(@PathVariable String userId) {
        if (!usersPayments.containsKey(userId)) {
            throw new NotFoundException("user not found");
        }

        return getUserPaymentAnalytic(userId);
    }

    @GetMapping("{userId}/stats")
    public UserPaymentStats getUserStats(@PathVariable String userId) {
        if (!usersPayments.containsKey(userId)) {
            throw new NotFoundException("user not found");
        }

        return getUserStatsImpl(userId);
    }

    @GetMapping("{userId}/templates")
    public List<UserPaymentTemplates> getUserTemplates(@PathVariable String userId) {
        if (!usersPayments.containsKey(userId)) {
            throw new NotFoundException("user not found");
        }

        return getUserTemplatesImpl(userId);
    }

    private List<UserPaymentTemplates> getUserTemplatesImpl(String userId) {
        List<UserPaymentTemplates> list = new ArrayList<>();

        Multimap<String, KafkaPaymentInfo> templates = HashMultimap.create();

        usersPayments.get(userId).forEach(kafkaPaymentInfo -> {
            String key = String.format("%s#%s#%s", kafkaPaymentInfo.getRecipientId(), kafkaPaymentInfo.getCategoryId(), kafkaPaymentInfo.getAmount().toString());
            templates.put(key, kafkaPaymentInfo);
        });

        for (String template : templates.keySet()) {
            if (templates.get(template).size() >= 3) {
                KafkaPaymentInfo next = templates.get(template).iterator().next();

                UserPaymentTemplates userPaymentTemplates = new UserPaymentTemplates();
                userPaymentTemplates.setRecipientId(next.getRecipientId());
                userPaymentTemplates.setCategoryId(next.getCategoryId());
                userPaymentTemplates.setAmount(next.getAmount());

                list.add(userPaymentTemplates);
            }
        }

        return list;
    }

    private UserPaymentStats getUserStatsImpl(String userId) {
        UserPaymentAnalytic userPaymentAnalytic = getUserPaymentAnalytic(userId);

        Map<String, PaymentCategoryInfo> analyticInfo = userPaymentAnalytic.getAnalyticInfo();
        int maxAmountCategoryId = -1;
        int minAmountCategoryId = -1;
        BigDecimal maxAmountCategory = BigDecimal.ZERO;
        BigDecimal minAmountCategory = new BigDecimal(Long.MAX_VALUE);

        int oftenCategoryId = -1;
        int rareCategoryId = -1;
        int oftenCategorySize = 0;
        int rareCategorySize = Integer.MAX_VALUE;

        for (String cid : analyticInfo.keySet()) {
            PaymentCategoryInfo info = analyticInfo.get(cid);
            int categoryId = Integer.parseInt(cid);

            if (oftenCategorySize < info.getSize()) {
                oftenCategorySize = info.getSize();
                oftenCategoryId = categoryId;
            }

            if (rareCategorySize > info.getSize()) {
                rareCategorySize = info.getSize();
                rareCategoryId = categoryId;
            }

            if (maxAmountCategory.compareTo(info.getSum()) < 0) {
                maxAmountCategory = info.getSum();
                maxAmountCategoryId = categoryId;
            }

            if (minAmountCategory.compareTo(info.getSum()) > 0) {
                minAmountCategory = info.getSum();
                minAmountCategoryId = categoryId;
            }
        }

        UserPaymentStats userPaymentStats = new UserPaymentStats();
        userPaymentStats.setOftenCategoryId(oftenCategoryId);
        userPaymentStats.setRareCategoryId(rareCategoryId);
        userPaymentStats.setMaxAmountCategoryId(maxAmountCategoryId);
        userPaymentStats.setMinAmountCategoryId(minAmountCategoryId);

        return userPaymentStats;
    }

    private UserPaymentAnalytic getUserPaymentAnalytic(String userId) {
        Map<String, PaymentCategoryInfo> analyticInfo = new HashMap<>();
        BigDecimal totalSum = BigDecimal.ZERO;

        Multimap<Integer, KafkaPaymentInfo> categories = HashMultimap.create();
        usersPayments.get(userId).forEach(kafkaPaymentInfo -> categories.put(kafkaPaymentInfo.getCategoryId(), kafkaPaymentInfo));

        for (Integer categoryId : categories.keySet()) {
            BigDecimal min = new BigDecimal(Long.MAX_VALUE);
            BigDecimal max = BigDecimal.ZERO;
            BigDecimal sum = BigDecimal.ZERO;

            for (KafkaPaymentInfo kafkaPaymentInfo : categories.get(categoryId)) {
                BigDecimal amount = kafkaPaymentInfo.getAmount();
                totalSum = totalSum.add((amount));
                sum = sum.add(amount);
                if (min.compareTo(amount) > 0) {
                    min = amount;
                }

                if (max.compareTo(amount) < 0) {
                    max = amount;
                }
            }

            PaymentCategoryInfo paymentCategoryInfo = new PaymentCategoryInfo(min, max, sum, categories.get(categoryId).size());
            analyticInfo.put(String.valueOf(categoryId), paymentCategoryInfo);
        }

        UserPaymentAnalytic userPaymentAnalytic = new UserPaymentAnalytic();
        userPaymentAnalytic.setUserId(userId);
        userPaymentAnalytic.setTotalSum(totalSum);
        userPaymentAnalytic.setAnalyticInfo(analyticInfo);

        return userPaymentAnalytic;
    }

    @KafkaListener(topics = "RAW_PAYMENTS", groupId = "main")
    public void listen(ConsumerRecord<String, String> cr) throws Exception {
        try {
            KafkaPaymentInfo paymentInfo = objectMapper.readValue(cr.value(), KafkaPaymentInfo.class);
            usersPayments.put(paymentInfo.getUserId(), paymentInfo);
        } catch (Throwable t) {
        }
    }
}
