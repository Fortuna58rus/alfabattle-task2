package com.alfabattle.task2.controller;

import com.alfabattle.task2.dto.HealthDTO;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/admin")
public class AdminController {

    @GetMapping("/health")
    public HealthDTO getHealth() {
        HealthDTO healthDTO = new HealthDTO();
        healthDTO.setStatus("UP");
        return healthDTO;
    }
}
